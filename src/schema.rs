// @generated automatically by Diesel CLI.
#![allow(non_snake_case)]
use diesel::table;

table! {
    Files (id) {
        id -> Integer,
        path -> Text,
        // I have no clue how to make this use the sqlite type.
        mtime -> Integer,
        size -> Integer,
    }
}
