/*! Scans the filesystem. */

use crate::astream::{ spawn_producer, Producer };
use std::path::{ Path, PathBuf };
//use std::time::SystemTime;
use time::PrimitiveDateTime;
use walkdir;
use walkdir::WalkDir;


#[derive(Debug)]
pub struct Metadata {
    pub path: PathBuf,
    pub mtime: PrimitiveDateTime,
    //mtime: SystemTime,
    pub size: usize,
}

pub fn scan(path: impl AsRef<Path>, bound: usize) -> Producer<Metadata> {
    spawn_producer(
        WalkDir::new(path)
            .follow_links(true)
            .into_iter()
            .filter_map(Result::ok)
            .filter_map(|entry| {
                // Metadata call accesses the filesystem, so better leave it in the separate thread.
                entry.metadata()
                    .ok()
                    .map(|meta| Metadata {
                        path: entry.path().to_path_buf(),
                        mtime: meta.modified().unwrap().into(),
                        size: meta.len() as usize,
                    })
            }),
        bound
    )
}
