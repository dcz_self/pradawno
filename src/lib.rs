#[macro_use] extern crate diesel;

pub mod astream;
pub mod basic_db;
pub mod consumator;
pub mod fs;
mod schema;
pub mod xdg;
