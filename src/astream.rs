/*! Asynchronous streaming from another thread. */

use crate::consumator::Consumator;
use std::sync::mpsc;
use std::thread;


/// Stops the iterator thread before sending the next message.
pub struct Stop;

pub enum Message<T> {
    Payload(T),
    Finished,
    Broken,
}

/// An iterator living in a separate thread.
pub struct Producer<T> {
    pub control: mpsc::Sender<Stop>,
    data: mpsc::Receiver<Option<T>>,
}

impl<T> Producer<T> {
    /// Get the nest message.
    pub fn next(&self) -> Message<T> {
        use Message::*;
        match self.data.recv() {
            Err(_) => Broken,
            Ok(None) => Finished,
            Ok(Some(v)) => Payload(v),
        }
    }
}

pub fn spawn_producer<T, I>(iter: I, bound: usize) -> Producer<T>
    where
        T: 'static + Send,
        I: 'static + Iterator<Item=T> + Send,
{
    let (control_send, control_recv) = mpsc::channel();
    let (data_send, data_recv) = mpsc::sync_channel(bound);
    thread::spawn(move || {
        for entry in iter {
            if let Ok(Stop) = control_recv.try_recv() {
                break;
            }
            data_send.send(Some(entry)).unwrap();
        }
        data_send.send(None).unwrap();
    });
    Producer {
        control: control_send,
        data: data_recv,
    }
}


/// A consumator living in a separate thread.
pub struct Consumer<T, Out> {
    /// If message is None, the Consumer will stop processing.
    data: mpsc::SyncSender<Option<T>>,
    thread: thread::JoinHandle<Out>,
}

type SendResult<T> = Result<(), mpsc::SendError<Option<T>>>;

impl<T, Cr> Consumator<T> for Consumer<T, Cr> {
    type NextResult = SendResult<T>;
    type CollectResult = thread::Result<Cr>;
    /// Push the next message.
    fn next(&mut self, message: T) -> SendResult<T> {
        self.data.send(Some(message))
    }

    fn collect(self) -> thread::Result<Cr> {
        // FIXME: unwrap
        self.data.send(None).unwrap();
        self.thread.join()
    }
}

/// Bound is for back pressure.
pub fn spawn_consumer<T, C, R, Cr>(mut following: C, bound: usize)
    -> Consumer<T, Cr>
    where
        T: 'static + Send,
        R: Send,
        Cr: 'static + Send,
        C: 'static + Consumator<T, CollectResult=Cr, NextResult=R> + Send,
{
    let (data_send, data_recv) = mpsc::sync_channel(bound);
    let handle = thread::spawn(move || {
        while let Some(value) = data_recv.recv().unwrap() {
            // The value is dropped. W/e.
            following.next(value);
        }
        // Notify the Consumator that no more data will arrive.
        // This could have been done on Consumator's Drop,
        // but I'm not sure if generators can handle Drop easily.
        following.collect()
    });
    Consumer {
        data: data_send,
        thread: handle,
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::time::Duration;
    
    #[test]
    fn async_stop() {
        let producer = spawn_producer(0..10, 3);
        assert_eq!(Ok(Some(0)), producer.data.recv());
        
        // Let the thread fill the buffer.
        thread::sleep(Duration::new(1, 0));
        
        producer.control.send(Stop).unwrap();

        // Not sure why one more gets through than the buffer size.
        assert_eq!(Ok(Some(1)), producer.data.recv());
        assert_eq!(Ok(Some(2)), producer.data.recv());
        assert_eq!(Ok(Some(3)), producer.data.recv());
        assert_eq!(Ok(Some(4)), producer.data.recv());
        assert_eq!(Ok(None), producer.data.recv());
    }
    
    #[test]
    fn async_consume() {
        let collector = Vec::new();
        let mut consumer = spawn_consumer(collector, 3);
        for i in 0..5 {
            consumer.next(i).unwrap();
        }
        assert_eq!(consumer.collect().unwrap(), vec![0, 1, 2, 3, 4]);
    }
}
