/*! Stores file metadata, so far */

/*! Metadata storage */
use crate::consumator::Consumator;
use crate::fs;
use crate::schema;
use diesel::sqlite::SqliteConnection;
use std::path::Path;
use std::sync::{Arc, Mutex};


use diesel::{ Connection as DConnection, ExpressionMethods, RunQueryDsl, QueryDsl };


pub struct Connection(Arc<Mutex<SqliteConnection>>);

impl Connection {
    pub fn new<P: AsRef<Path>>(path: P) -> Self {
        Connection(Arc::new(Mutex::new(
            SqliteConnection::establish(path.as_ref().to_str().unwrap())
                .unwrap()
        )))
    }

    pub fn multi_inserter(&self) -> MultiInserter {
        MultiInserter {
            connection: self.0.clone(),
        }
    }
}

pub struct MultiInserter {
    connection: Arc<Mutex<SqliteConnection>>,
}

impl Consumator<Vec<fs::Metadata>> for MultiInserter {
    type NextResult = ();
    type CollectResult = ();

    fn next(&mut self, data: Vec<fs::Metadata>) {
        use schema::Files::dsl::*;
        let values: Vec<insert::Data>
            = data.into_iter().map(insert::Data::from).collect();
        let conn = self.connection.lock().unwrap();
        diesel::insert_into(schema::Files::table)
            .values(values)
            .execute(&*conn)
            .expect(&format!("Failed to insert"));
    }

    fn collect(self) -> () {}
}

/*
struct Inserter {
    connection: Arc<Mutex<SqliteConnection>>,
    queue: Vec<fs::Metadata>,
    batch_size: usize,
}

impl Inserter {
    fn flush(&mut self) {
        table.foo(self.queue);
        self.queue = Vec::new();
    }

    pub fn next(&mut self, data: fs::Metadata) {
        self.queue.push(data);
        if self.queue.len() >= self.batch_size {
            self.flush();
        }
    }
}

impl Drop for Inserter {
    fn drop(&self) {
        self.flush();
    }
}*/


/* Same, just with generators. Maybe todo later.
fn batch_insert(connection: Arc<Mutex<SqliteConnection>>) -> Consumator<fs::Metadata> {
    while let Some(data) = yield {
        connection.insert(data);
    }
}

fn batch<T>(gen: Consumator<T>, batch_size: usize) -> Consumator<Vec<T>> {
    let mut queue = Vec::new();
    while let Some(data) = yield {
        queue.push(data);
        if queue.len() >= batch_size {
            gen.next(queue);
            queue = Vec::new();
        }
    }
    if queue.len() > 0 {
        gen.next(queue);
    }
}
*/


pub mod insert {
    use super::*;
    
    use crate::schema::Files;
    
    #[derive(Insertable)]
    #[table_name="Files"]
    pub struct Data {
        path: String,
        mtime: i32,
        size: i32,
    }
    
    impl From<fs::Metadata> for Data {
        fn from(meta: fs::Metadata) -> Data {
            Data {
                // FIXME: save it as byte stream instead?
                path: meta.path.to_string_lossy().into(),
                mtime: meta.mtime.assume_utc().unix_timestamp() as i32,
                size: meta.size as i32,
            }
        }
    }
}
/*
pub mod query {
    use super::*;
    struct Data {
    }

    impl From<Data> for fs::Metadata {
        fn from(meta: Data) -> fs::Metadata {
            todo!()
        }
    }
    
    fn query(mut connection: &mut Connection, photos: FilteredType)
        -> Result<Vec<fs::Metadata>, ()>
    {
        use schema::PhotoMeta::dsl::*;
        //Entries
            //.filter(start_time.gt((Utc::now() - Duration::days(32)).timestamp() as i32))
        photos
            .order(time_created.desc())
            .load::<Data>(connection)
            .unwrap()
            .into_iter()
            .map(fs::Metadata::from)
            .collect()
    }
}
*/


