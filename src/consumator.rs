/*! Consumator and basic adapters. */

use std::mem;

/// The dual to Iterator: instead of producing values, consumes them.
/// Side effects of course allowed.
// R is meant to be either () or a Result.
// The Result also only makes sense for the spawned Consumator,
// where data may not have anywhere to go.
// Maybe the spawned Consumator should not use the Consumator interface…
pub trait Consumator<T> {
    type NextResult;
    type CollectResult;
    /// Feeds another item.
    fn next(&mut self, value: T) -> Self::NextResult;
    /// Ends processing, returning the result.
    fn collect(self) -> Self::CollectResult;
}

impl<T> Consumator<T> for Vec<T> {
    type NextResult = ();
    type CollectResult = Vec<T>;
    fn next(&mut self, value: T) -> () {
        self.push(value);
    }
    fn collect(self) -> Vec<T> {
        self
    }
}

/// Forwards T in Vec<T> batches of desired size.
/// *Must* be collected before finishing, or the last batch will be lost.
pub struct Batcher<T, C: Consumator<Vec<T>>> {
    forward: C,
    queue: Vec<T>,
    batch_size: usize,
}

impl<T, C: Consumator<Vec<T>>> Batcher<T, C> {
    pub fn feed_to(batch_size: usize, forward: C) -> Self {
        Batcher {
            forward,
            queue: Vec::new(),
            batch_size,
        }
    }

    fn flush(&mut self) -> C::NextResult {
        let batch = mem::replace(&mut self.queue, Vec::new());
        self.forward.next(batch)
    }
}

impl<T, Cr, R, C> Consumator<T> for Batcher<T, C>
    where
        R: Default,
        C: Consumator<Vec<T>, CollectResult=Cr, NextResult=R>,
{
    type NextResult = R;
    type CollectResult = Cr;
    fn next(&mut self, value: T) -> R {
        self.queue.push(value);
        if self.queue.len() >= self.batch_size {
            self.flush()
        } else {
            R::default()
        }
    }
    fn collect(mut self) -> Cr {
        // TODO: this drops the result
        self.flush();
        self.forward.collect()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn batcher() {
        let mut consumer = Batcher::feed_to(3, Vec::new());
        for i in 0..7 {
            consumer.next(i);
        }
        assert_eq!(
            consumer.collect(),
            vec![
                vec![0, 1, 2],
                vec![3, 4, 5],
                vec![6],
            ],
        );
    }
}
