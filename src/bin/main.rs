/*! Stores information about files into the database.
 * 
 * This file is the imperative shell over two streams.
 * 
 * Create the database as DATABASE_PATH:
 * ```
 * sqlite3 db.sqlite < pradawno/db.sql
 * ```
 * Then run this example:
 * 
 * ```
 * DATABASE_PATH=../db.sqlite cargo run
 * ```
 */

use pradawno::astream;
use pradawno::basic_db;
use pradawno::consumator::Batcher;
use pradawno::fs;
use pradawno::xdg;

use std::env;
use std::path::PathBuf;

// Traits
use pradawno::consumator::Consumator;


fn make_connection() -> basic_db::Connection {
    let db_path = env::var("DATABASE_PATH")
        .ok()
        .map(PathBuf::from)
        .or(xdg::data_path("./files.db"))
        .unwrap();
    basic_db::Connection::new(db_path)
}

fn main() {
    let scanner = fs::scan(".", 10);
    let db = make_connection();
    let inserter = db.multi_inserter();
    let inserter = Batcher::feed_to(10, inserter);
    
    let mut db_insert = astream::spawn_consumer(inserter, 10);
    
    while let astream::Message::<_>::Payload(msg) = scanner.next() {
        println!("{:?}", msg);
        db_insert.next(msg).unwrap();
    }
}
