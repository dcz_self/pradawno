# Pradawno

Goal: a photo manager based on queries.

Progress: Stores all files with some info inside a sqlite database.

## Technical goal

Explore "Imperative shell, functional core". See examples, and main.rs.

"Functional core" makes easy sense when the state is known and small. In the photo manager case, the state is not very functional.

The database is used in order to let data be huge. The filesystem is already huge, but also can be mutated by outside sources. So in both cases, the state can't be held within the program, and those pieces can't be treated as functional cores.

Instead, they are pushed to the outside of the hexagon, and treated as event sources or sinks (streams). Notably, they have very little control flow.

Control flow is pushed out to functional tools composing with the event streams, like map(), etc. See the Consumator trait. Those functional tools can be asdequately unit tested.

The imperative shell wraps the functional pieces over the imperative streams. So far, it's very simple and doesn't contain much in the way of control flow, making testing less relevant.

## Copying

This code is licensed under the AGPL version 3.0 or later.
