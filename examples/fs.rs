use pradawno::astream;
//mod astream;
use pradawno::fs;
//mod meta_db;
//mod metadata;

fn main() {
    let scanner = fs::scan(".", 10);
    while let astream::Message::Payload(msg) = scanner.next() {
        println!("{:?}", msg);
    }
}
